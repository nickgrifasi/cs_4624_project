import os
import pandas as pd
import csv
import re
from datetime import datetime as dt, timedelta
from collections import defaultdict
import time

# Initializing some variables
avg_session_time = 0
total_session_count = 0
total_session_time = 0

document_event = ['document-ready']
window_event = ['window-unload', 'window-blur', 'window-focus', ]
pe_event = ['jsav-matrix-click', 'jsav-exercise-grade', 'jsav-exercise-reset', 'jsav-node-click', 
                'button-identifybutton', 'button-editbutton', 'button-addrowbutton', 'button-deletebutton', 'button-setterminalbutton', 'button-addchildbutton',
                'button-checkbutton', 'button-autobutton', 'button-donebutton',
                'submit-helpbutton', 'submit-edgeButton', 'submit-deleteButton', 'submit-undoButton', 'submit-redoButton', 'submit-editButton', 'submit-nodeButton',
                'submit-begin', 'submit-finish', 'button-hintbutton', 'button-movebutton', 'button-removetreenodebutton', 'button-savefile', 'button-edgebutton', 
                'jsav-exercise-model-end', 'jsav-exercise-model-begin', 'jsav-array-click',
                'jsav-exercise-gradeable-step', 'jsav-exercise-grade',
                'jsav-exercise-model-open', 'jsav-exercise-model-forward',
                'jsav-exercise-model-close', 'jsav-node-click',
                'jsav-exercise-grade-change', 'jsav-exercise-reset',
                'jsav-exercise-step-fixed', 'jsav-arraytree-click',
                'jsav-exercise-undo', 'jsav-exercise-model-backward',
                'jsav-exercise-model-begin', 'jsav-exercise-step-undone',
                'jsav-exercise-model-end', 'odsa-award-credit', 'odsa-exercise-init', 
                'button-classify', 'button-throwRoll', 'button-calculate',
                'button-decrement', 'button-help', 'button-selecting',
                'button-sorting', 'button-incrementing', 'button-run',
                'button-partition', 'button-markSorted', 'button-reset',
                'button-outputbuffer', 'button-noaction', 'button-submit',
                'button-insert', 'button-remove', 'button-next', 'button-about',
                'button-undir', 'button-dir', 'button-clear', 'button-read',
                'button-write', 'button-restart']
ff_event = ['jsav-begin', 'jsav-end', 'jsav-forward', 'jsav-backward']
other_event = ['hyperlink', 'jsav-narration-on', 'jsav-narration-off', 'button-layoutRef', 'odsa-exercise-init']

def readfile(raw_data):
    os.chdir("./data/" + raw_data)
    print("Reading the raw_data . . .")
    df = pd.read_csv(raw_data + "_sorted.csv")
    global csvdata
    csvdata = df.sort_values(['user_id', 'action_time'])
    print(df.iloc[1])
    

def main(raw_data):
    readfile(raw_data)